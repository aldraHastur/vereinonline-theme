# Deployment-Strategie
## S3-Bucket
Die `master`und `dev` Branches werden über einen S3-Bucket zu Verfügung gestellt.

- `dev`: https://wjd-frontend-vereinonline-theme.s3.eu-central-1.amazonaws.com/dev/WJ-2021.zip
- `master`: https://wjd-frontend-vereinonline-theme.s3.eu-central-1.amazonaws.com/master/WJ-2021.zip

## Deployment-Script
Um das Deployment zu automatisieren kann ein Shell-Script genutzt werden. Es ist je nach Instanz notwendig die Pfadangabe zum Layout-Verzeichnis anzupassen! Dies gilt auch für Benutzer und Gruppe im chown Befehl!

```bash
#!/bin/bash
cd /tmp
mkdir wjlayout
cd wjlayout
wget https://wjd-frontend-vereinonline-theme.s3.eu-central-1.amazonaws.com/dev/WJ-2021.zip
unzip WJ-2021.zip
rm WJ-2021.zip
wget https://wjd-frontend-vereinonline-theme.s3.eu-central-1.amazonaws.com/master/WJ-2021.zip
unzip WJ-2021.zip
rm WJ-2021.zip
rm -rf /opt/bitnami/apache2/htdocs/admin/layout/WJ-2021-DEV/*
cp -r ./WJ-2021-DEV/* /opt/bitnami/apache2/htdocs/admin/layout/WJ-2021-DEV/
chown -R bitnami:bitnami /opt/bitnami/apache2/htdocs/admin/layout/WJ-2021-DEV/
rm -rf /opt/bitnami/apache2/htdocs/admin/layout/WJ-2021/*
cp -r ./WJ-2021-DEV/* /opt/bitnami/apache2/htdocs/admin/layout/WJ-2021/
chown -R bitnami:bitnami /opt/bitnami/apache2/htdocs/admin/layout/WJ-2021/
rm -rf /tmp/wjlayout
```

Dieses Skript kann beispielsweise im Home-Verzeichnis des Benutzers abgelegt werden (wj-layout-deploy.sh). Um das Skript ausführen zu können muss es ausführbar gemacht werden (chmod u+x wj-layout-deploy.sh).

## Cronjob
Um immer die aktuelle Version zu Verfügung zu haben sollte ein Cronjob installiert werden. Dies erfolgt durch das Einfügen der Zeile 
``` 
* 2 * * * /home/bitnami/wj-layout-deploy.sh >/dev/null 2>&1
```
mittels des Befehls `crontab -e`.

Somit wird immer nachts um 2 Uhr ein Update ausgeführt.