# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.2.3] - 2023-01-19
### Fixed
- issue #208: Karten-Links bei vertikalen Karten am unteren Ende der Karte orientiert

## [1.2.3] - 2023-01-19
### Fixed
- issue #205: Karten in vertikaler 33% Ansicht gefixt

## [1.2.2] - 2023-01-12
### Fixed
- issue #205: Karten unter Aktuelles aufgeteilt in ein 50/50 Layout

## [1.2.1] - 2022-07-28
### Fixed
- issue #188: Mobile Menü Punkte ohne Untermenü nicht mehr doppelt anklickbar

## [1.2.0] - 2022-07-01
### Added
- #75 Kartensammlungen: Karten haben auf Desktop pro Zeile die gleiche Höhe
- #74 Kartensammlungen: Gleiche höhe für Bilder
- #102 Smartcrop für Bilder: Bilder werden kontextbasiert korrekt in den Ausschnitt eingefügt
- #39 Sektion Mitglieder angelegt um Kartensammlung 33/33/33 dynamisch auf Mitglieder anzuwenden

### Changed
- #99 Autoplay bei mp4 Einbettung in Video Element
- #178 Hinzufügen-Button bei Veranstaltung/Aktuelles dupliziert
- #174 Events in Listenansicht öffnen sich jetzt auch in neuem Fenster
- #177 Bildhöhe für Bilder in HTML Blöcken auf auto gesetzt, so dass diese korrekt skaliert werden können
- #114 Edit Link eingefügt für Veranstaltungen -> bei Aktuelles steht noch aus

### Fixed
- #93 zweizeiliger Text bei Terminblock Aktuelles/Events

## [1.1.3] - 2022-04-27
### Added
- favicon set from Tobias Uwe Kiefer (tobias@kiefer.one) for #141 and #171
- yall.js for #159: Safari Lazy Loading per yall.js in Listenansicht

### Changed
- Dokumentation angepasst: Name von Startseite muss "start" lauten (siehe #115)
- Frontend Build System für Node.JS 16 Kompatibilität aktualisiert

### Fixed
- issue #173: Link in Footer Logo navigiert nun zu "./"
- issue #165: Link für Monatskalender navigiert nun zu "./"
- issue #160: (ebenso #106) line-clamp für Aktuelles und AktuellesEvents


## [1.1.2] - 2022-03-30
### Added
- issue #159: Lazy Loading für Veranstaltungs-Sektion Bilder. Native Lazy Load gem. https://imagekit.io/blog/lazy-loading-images-complete-guide/#javascript-dependency-of-lazy-loading

## [1.1.1] - 2022-02-13
### Fixed
- issue #143: JavaScript Problem betreffend der Monatsumschaltung auf Unterseiten behoben

## [1.1.0] - 2022-02-11
### Fixed
- issue #139/#147: snippets aus HTML Template für falsche Verlinkung entfernt
- issue #103: clamp-body auf Paragraph angewendet (für Firefox/Safari) - kein überlappender Text mehr in Aktuelles/AktuellesEvents
- issue #104: Zeilenumbruch bei Partner

### Added
- issue #118: Konfiguration um Partner-Bereich auszublenden

## [1.0.3] - 2021-09-23
### Fixed
- issue #115: Kalendereinbindung auf Startseite sorgte dafür, dass JS fehlschlägt, dysfunktion Monatskalender. 

## [1.0.2] - 2021-06-08
### Added
- issue #90: Footer Social Icon für Facebook

### Changed
- issue #98: Autoplay und Pagination bei HeroSlider

### Fixed
- issue #92: Bild in Kartensammlung wurde nur bei eingetragenem Link angezeigt