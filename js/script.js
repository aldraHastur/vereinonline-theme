/* eslint-disable linebreak-style */
/* eslint-disable no-undef */
jQuery(document).ready(function () {
    /**
     * Global Vars
     */
    var body = jQuery('body');
    /**
     * Apply IE Styles
     */
    if (getInternetExplorerVersion() !== -1){
        jQuery('body').addClass('ie');
        if (jQuery(window).width() > 768) {
            var cards = jQuery('.card');
            cards.each(function(k, value) {
                value = jQuery(value);
                // eslint-disable-next-line no-undef
                imagesLoaded(value, function() {
                    value = value.find('img');
                    if (value.length > 0) {
                        var valueParent = value.parent();
                        if (value.outerWidth() / value.outerHeight() > valueParent.outerWidth() / valueParent.outerHeight()) {
                            value.css('max-height', '100%');
                        } else {
                            value.css('max-width', '100%');
                            valueParent.css('min-height', value.outerHeight());
                        }
                    }                    
                });
            });
        }
    }
    var divcalenedar = jQuery('#divkalender');
    if (divcalenedar.length > 0) {
        divcalenedar.each(function(key, value) {
            value = jQuery(value);
            initCalendarForm(value.prev('form'));
        });
    }
    var mapforms = jQuery('#map-canvas');
    if (mapforms.length > 0) {
        mapforms.each(function(key, value) {
            value = jQuery(value);
            initCalendarForm(value.prev('script').prev('form'));
        });
    }
    var fullCalendarForms = jQuery('form #divkalender');
    if (fullCalendarForms.length > 0) {
        fullCalendarForms.each(function(key, value) {
            value = jQuery(value).closest('form');
            value.addClass('search-calendar-form');
            value.find('select').select2({
                minimumResultsForSearch: -1
            })
        })
    }
    function initCalendarForm(form) {        
        if (form.length > 0) {
            form.addClass('calendar-form');
            form.find('select').select2({
                minimumResultsForSearch: -1
            })
        }
    }
    /**
     * Nav Menus
     */
     var mainNavItems = jQuery('.main-nav__item.menu-item-has-children');
     var subNav = jQuery('.header__sub-nav-wrapper');
     if (mainNavItems.length > 0) {
         mainNavItems.click(function(e) {
             e.preventDefault();
             var target = jQuery(e.target);
             var link = target.find('>a');
             var clickedIndex = target.index() + 1;
             if (link.hasClass('is-selected')) {
                 //Only Collapse current Menu
                 link.removeClass('is-selected');
                 subNav.removeClass('is-open');
                 subNav.find('.is-selected').removeClass('is-selected');
             } else {
                 //Check if there is another menu open, if so close it
                 if (mainNavItems.find('.is-selected').length > 0) {
                     mainNavItems.find('.is-selected').removeClass('is-selected');
                     subNav.find('.is-selected').removeClass('is-selected');
                 }
                 //Open current menu
                 subNav.addClass('is-open');
                 link.addClass('is-selected');
                 subNav.find('.sub-nav:nth-child('+clickedIndex+')').addClass('is-selected');
             }
         });
         jQuery('.header__close').click(function() {
             mainNavItems.find('.is-selected').removeClass('is-selected');
             subNav.removeClass('is-open');
             subNav.find('.is-selected').removeClass('is-selected');
         });
     }

    var mobileNav = jQuery('.mobile-nav');
    if (mobileNav.length > 0) {
        var jsHeader = jQuery('.js-header');
        var headerToggle = jQuery('.header__toggle');
        headerToggle.click(function() {
            if (jsHeader.hasClass('is-open')) {
                jsHeader.removeClass('is-open');
                jsHeader.addClass('is-closed');
                headerToggle.removeClass('is-active');
            } else {
                jsHeader.removeClass('is-closed');
                jsHeader.addClass('is-open');
                headerToggle.addClass('is-active');
            }
        });
        var menuLinks = mobileNav.find('li.mobile-nav__item');
        menuLinks.each(function(k, value) {
            value = jQuery(value);
            var valueLink = value.find('>a');
            value.click(function(e) {
                var target = jQuery(e.target);
                e.stopPropagation();
                if (value.hasClass('is-not-selected')) {
                    value.removeClass('is-not-selected');
                    valueLink.removeClass('is-not-selected');
                    value.addClass('is-selected');
                    valueLink.addClass('is-selected');
                    jQuery('body, html').animate({
                        scrollTop: value.offset().top
                    }, 400);
                } else {
                    if (target.hasClass('is-selected')) {
                        value.removeClass('is-selected');
                        valueLink.removeClass('is-selected');
                        value.addClass('is-not-selected');
                        valueLink.addClass('is-not-selected');
                    }
                }
            });
        });
    }
    /**
     * Hero Swiper
     */
    var heroSwiper = jQuery('.hero-swiper');
    if (heroSwiper.length > 0) {
        heroSwiper.each(function(k, swiper) {
            swiper = jQuery(swiper);            
            if (jQuery(window).width() < 992) {
                heroSwiper.find('.swiper-slide').outerHeight(heroSwiper.data('height-resp')+'px');
            } else {
                heroSwiper.find('.swiper-slide').outerHeight(heroSwiper.data('height')+'px');
            }
            if (swiper.find('.swiper-slide').length > 1) {
                new Swiper('.hero-swiper', {
                    direction: 'horizontal',
                    effect: 'slide',
                    freeModeSticky: true,
                    loop: true,
                    centeredSlides: true,
                    freeMode: false,
                    pagination: {
                        el: '.hero__pagination',
                        clickable: true,
                        renderBullet: function (index, className) {
                            return '<li class="hero__pagination-item ' + className + '"></li>';
                        }
                    },
                    autoplay: {
                        delay: 5000,
                        disableOnInteraction: false
                    }
                });
            }
        })
    }
    /**
     * Image Gallery Lightbox
     */
    var imageGallery = jQuery('.image-gallery.is-desktop');
    if (imageGallery.length > 0) {
        imageGallery.each(function(k, swiperValue) {
            swiperValue = jQuery(swiperValue);
            swiperValue.find('.gallery-swiper').addClass('gallery-swiper-'+k);
            var overlay = swiperValue.find('.image-gallery-overlay');
            var gallerySwiper = new Swiper('.gallery-swiper-'+k, {
                direction: 'horizontal',
                loop: true,
            });
            swiperValue.find('.image-gallery__thumbnail').each(function(k, value) {
                value = jQuery(value);
                value.click(function() {
                    var openIndex = value.data('index-image');
                    gallerySwiper.slideTo(openIndex,0,false);
                    overlay.removeClass('invis');
                    body.addClass('is-modal-open');
                });
            });
            swiperValue.find('.image-gallery__thumbnail-redirect').click(function() {
                swiperValue.find('.image-gallery__thumbnail').first().trigger('click');
            });
            overlay.find('.image-gallery-overlay__close').click(function() {
                overlay.addClass('invis');
                body.removeClass('is-modal-open');
            });
            overlay.find('.image-gallery-overlay__next').click(function() {
                gallerySwiper.slideNext();
            });
            overlay.find('.image-gallery-overlay__prev').click(function() {
                gallerySwiper.slidePrev();
            });
        });
    }
    var mobileImageGallery = jQuery('.image-gallery.is-mobile');
    if (mobileImageGallery.length > 0) {
        mobileImageGallery.each(function(k, mobGalValue) {
            mobGalValue = jQuery(mobGalValue);
            mobGalValue.find('.swiper-container').addClass('gallery-swiper-'+k);
            mobGalValue.find('.toggle').click(function(e) {
                e.preventDefault();
                var slidesJSON = JSON.parse(mobGalValue.attr('data-images'));
                var swiperWrapper = mobGalValue.find('.swiper-wrapper');
                swiperWrapper.empty();
                jQuery.each(slidesJSON, function(k, JSONvalue) {
                    if (JSONvalue.src) {
                        swiperWrapper.append('<div class="image-gallery__slide swiper-slide"><div class="image-gallery__thumbnail"><figure class="figure"><div class="figure__image-wrapper is-highlighted"><img src="'+JSONvalue.src+'" class="figure__image"></div><figcaption class="figure__caption"><div class="image-gallery__caption caption"><div class="image-gallery__caption-meta caption__meta">'+JSONvalue.meta+'</div><div class="image-gallery__caption-title caption__title">'+JSONvalue.caption+'</div></div></figcaption></figure></div></div>');
                    } else {
                        swiperWrapper.append('<div class="image-gallery__slide swiper-slide"><div class="image-gallery__thumbnail"><figure class="figure"><div class="figure__image-wrapper is-highlighted"><div class="figure__image">'+JSONvalue.image+'</div></div><figcaption class="figure__caption"><div class="image-gallery__caption caption"><div class="image-gallery__caption-meta caption__meta">'+JSONvalue.meta+'</div><div class="image-gallery__caption-title caption__title">'+JSONvalue.caption+'</div></div></figcaption></figure></div></div>');
                    }
                });
                new Swiper('.gallery-swiper-'+k, {
                    direction: 'horizontal',
                    loop: true,
                });
            });
        });
    }
    /**
     * JS Webforms
     */
    var jsWebform = jQuery('.js-webform');
    if (jsWebform.length > 0) {
        jsWebform.each(function(k, formValue) {
            formValue = jQuery(formValue);
            var requiredInputsMap = [];
            formValue.find('input[required], input[aria-required="true"]').each(function(k, reqValue) {
                reqValue = jQuery(reqValue);
                requiredInputsMap.push({value: reqValue, name: reqValue.attr('name')});
                reqValue.removeAttr('required');
            });            
            formValue.find('textarea[required], textarea[aria-required="true"]').each(function(k, reqValue) {
                reqValue = jQuery(reqValue);
                requiredInputsMap.push({value: reqValue, name: reqValue.attr('name')});
                reqValue.removeAttr('required');
            });
            formValue.submit(function(e) {
                e.preventDefault();
                var failedInputs = [];
                formValue.find('.is-invalid').removeClass('is-invalid');
                jQuery(requiredInputsMap).each(function(key) {
                    if (requiredInputsMap[key].value.attr('type') === 'radio') {
                        var sameNameFilter = requiredInputsMap.filter(function(x) {
                            return x.name === requiredInputsMap[key].name;
                        });
                        var isChecked = false;
                        jQuery(sameNameFilter).each(function(k, value) {
                            if (value.value.is(':checked')) {
                                isChecked = true;
                                return false;
                            }
                        });                        
                        if (!isChecked) {
                            requiredInputsMap[key].value.closest('.form-item').addClass('is-invalid');
                            requiredInputsMap[key].value.closest('.js-webform-radios').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.closest('.webform-flex--container').data('error'));
                        }
                    } else if (requiredInputsMap[key].value.attr('type') === 'checkbox') {
                        if (requiredInputsMap[key].value.is(':checked')) {
                            return false;
                        } else {
                            requiredInputsMap[key].value.closest('.js-form-type-checkbox').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.closest('.webform-flex--container').data('error'));
                        }
                    } else {
                        if (requiredInputsMap[key].value.val().length <= 0) {                            
                            requiredInputsMap[key].value.closest('.form-item').addClass('is-invalid');
                            failedInputs.push(requiredInputsMap[key].value.closest('.webform-flex--container').data('error'));
                        }
                    }
                });
                failedInputs = jQuery.uniqueSort(failedInputs);
                jQuery('.error-list').remove();
                if (failedInputs.length > 0) {
                    var errors = jQuery('<div>', {class: 'error-list'});
                    var errorList = jQuery('<ul>', {class: 'item-list__comma-list'});
                    if (failedInputs.length === 1) {
                        errors.text('1 Fehler wurde gefunden');
                    } else {
                        errors.text(failedInputs.length+' Fehler wurden gefunden');
                    }
                    errors.append(errorList);
                    jQuery.each(failedInputs, function(k, errorValue) {
                        errorList.append(jQuery('<li>', {text: errorValue}));
                    });
                    formValue.prepend(errors);
                    jQuery('body, html').animate({
                        scrollTop: errors.offset().top
                    }, 200);
                } else {
                    formValue[0].submit();
                }
            });
        });
    }
    /**
     * Cookie Banner TODO: remove this?
     */
    /*
    var cookieBanner = jQuery('#cookie-banner');
    if (cookieBanner.length > 0) {
        if (!getCookie('cookie-consent')) {
            cookieBanner.show();
        }
        cookieBanner.find('.accept').click(function() {
            setCookie('cookie-consent', true, 365);
            cookieBanner.hide();
        });
        cookieBanner.find('.cookie-banner__close').click(function() {
            cookieBanner.hide();
        });
    }
    */
    /**
     * Card Contets
     */
    var sectionImageHeights = jQuery('.section__content.set-image-height');
    if (sectionImageHeights.length > 0) {
        sectionImageHeights.each(function(key, value) {
            value = jQuery(value);
            value.find('.card__image-wrapper .figure__image').each(function(ik, innerValue) {
                innerValue = jQuery(innerValue);
                innerValue.css('height', value.attr('data-image-height')+'px');
                innerValue.find('img').css('height', value.attr('data-image-height')+'px');
            })
        }); 
    }

    var cardSections = jQuery('.section');
    if (cardSections.length > 0) {
        cardSections.each(function(k, value) {
            value = jQuery(value);
            var cardContents = value.find('.card.is-vertical .card__content');
            var cardImages = value.find('.card.is-vertical .figure__image');
            imagesLoaded(cardImages, function() {
                var maxHeight = 0;
                cardContents.each(function(k, value) {
                    value = jQuery(value);
                    var h = value.outerHeight();
                    if (h > maxHeight) {
                        maxHeight = h;
                    }
                });
                cardContents.each(function(k, value) {
                    value = jQuery(value);
                    value.css('height', maxHeight);
                });
                cardImages.each(function(k, value) {
                    var jqValue = jQuery(value);
                    var options = {
                        width: jqValue.outerWidth(),
                        height: jqValue.outerHeight()
                    }
                    var img = jqValue.find('img');
                    smartcrop.crop(img[0], options).then(function(result) {
                        var crop = result.topCrop;
                        var canvas = jQuery('<canvas>')[0];
                        var ctx = canvas.getContext('2d');
                        canvas.width = options.width;
                        canvas.height = options.height;
                        ctx.drawImage(img[0], crop.x, crop.y, crop.width, crop.height, 0, 0, canvas.width, canvas.height);
                        img.after(canvas);
                        img.remove();
                    });
                })
            });
        });
    }
    var clickCards = jQuery('.card');
    if (clickCards.length > 0) {
        clickCards.each(function(k, value) {
            value = jQuery(value);
            if (value.data('url')) {
                value.click(function(e) {
                    e.preventDefault();
                    window.location = value.data('url');
                });
            }
        });
    }
    /* TODO: remove?
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = 'expires='+ d.toUTCString();
        cvalue = JSON.stringify(cvalue);
        document.cookie = cname + '=' + cvalue + ';' + expires + '; path=/';
    }
    function getCookie(cname) {
        var name = cname + '=';
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i <ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) === ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return '';
    }*/

    /**
     * Check for IE (Better to be done at PHP Level)
     */
    function getInternetExplorerVersion() {
        var rV = -1;
        if (navigator.appName === 'Microsoft Internet Explorer' || navigator.appName === 'Netscape') {
            var uA = navigator.userAgent;
            var rE = new RegExp('MSIE ([0-9]{1,}[.0-9]{0,})');

            if (rE.exec(uA) != null) {
                rV = parseFloat(RegExp.$1);
            } else if (navigator.userAgent.match(/Trident.*rv:11\./)) {
                rV = 11;
            }
        }
        return rV;
    }
    
    Date.prototype.getWeekNumber = function(){
        var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
        var dayNum = d.getUTCDay() || 7;
        d.setUTCDate(d.getUTCDate() + 4 - dayNum);
        var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
        return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
    };

    Date.prototype.addDays = function(days) {
        var date = new Date(this.valueOf());
        date.setDate(date.getDate() + days);
        return date;
    }
    
    function getDates(startDate, stopDate) {
        var dateArray = new Array();
        var currentDate = startDate;
        while (currentDate <= stopDate) {
            dateArray.push(new Date (currentDate));
            currentDate = currentDate.addDays(1);
        }
        return dateArray;
    }

    var eventView = jQuery('.eventview');
    if (eventView.length > 0) {
        //General Controls
        var viewModes = eventView.find('.view-modes > .button');
        var listView = eventView.find('.list-view');
        var weekView = eventView.find('.week-view');
        var monthView = eventView.find('.month-view');
        var header = eventView.find('.event-overview-header');
        var defaultView = header.data('default-view');
        if (window.location.search.split('&monat=')[1]) { // overwrite default in Month view
            defaultView = 'Monat';
        }
        switch (defaultView) {
            case 'Woche': {
                listView.hide();
                monthView.hide();
                header.addClass('week');
                eventView.find('.view-modes > .week').addClass('active');
                break;
            }
            case 'Liste': {
                weekView.hide();
                monthView.hide();
                header.addClass('list');
                eventView.find('.view-modes > .list').addClass('active');
                break;
            }
            default: {
                weekView.hide();
                listView.hide();
                header.addClass('month');
                eventView.find('.view-modes > .month').addClass('active');
                break;
            }
        }
        
        viewModes.each(function(k, value) {
            value = jQuery(value);
            value.click(function(e) {
                e.preventDefault();
                if (!value.hasClass('active')) {
                    header.removeClass('week month list');
                    viewModes.removeClass('active');
                    value.addClass('active');
                    if (value.hasClass('list')) {
                        listView.show();
                        weekView.hide();
                        monthView.hide();
                        header.addClass('list');
                    }
                    if (value.hasClass('week')) {
                        listView.hide();
                        weekView.show();
                        monthView.hide();
                        header.addClass('week');
                    }
                    if (value.hasClass('month')) {
                        listView.hide();
                        weekView.hide();
                        monthView.show();
                        header.addClass('month');
                    }
                }
            })
        })

        //ListView
        var headlinePast = jQuery('.headline-past-events');
        if (headlinePast.length > 0) {
            if (headlinePast.next('.section').length <= 0) {
                headlinePast.remove();
            }
        }

        //WeekView
        //Variables Init
        var allEvents = weekView.data('dates').split(';;;');
        var weekEventsMarkup = weekView.find('.section');
        var cleanedEvents = new Array();
        var cleanedEventsByWeek = new Array();
        var today = new Date();
        var currentWeekNumber = today.getWeekNumber();
        var currentYear = today.getFullYear();
        var weekdays = eventView.find('.weekdays .weekday');
        var weekNavPrev = header.find('.week-nav .prev-week');
        var weekNavCurrent = header.find('.week-nav .current-week');
        var weekNavNext = header.find('.week-nav .next-week');
        var isLeapYear = ((currentYear % 4 == 0) && (currentYear % 100 != 0)) || (currentYear % 400 == 0);

        //Prep Nav
        weekNavPrev.attr('data-target-week', currentWeekNumber - 1);
        weekNavNext.attr('data-target-week', currentWeekNumber + 1);

        //Get cleaned Events
        allEvents.forEach(function(value, key) {
            if (value.length > 0) {
                if (value.substr(10,4) != currentYear) {
                    return;
                }
                var startDate = new Date(value.substr(10,4)+'-'+value.substr(7,2)+'-'+value.substr(4,2));
                if (value.indexOf('bis') >= 0) {
                    var endDate = new Date(value.substr(29,4)+'-'+value.substr(26,2)+'-'+value.substr(23,2));
                    var dates = getDates(startDate, endDate);
                    dates.forEach(function(value, key) {
                        cleanedEvents.push(value);
                        var weekNo = value.getWeekNumber();
                        if (cleanedEventsByWeek[weekNo]) {
                            cleanedEventsByWeek[weekNo].push({
                                id: key,
                                date:value
                            });
                        } else {
                            cleanedEventsByWeek[weekNo] = [{
                                id: key,
                                date:value
                            }];
                        }
                    });
                } else {
                    cleanedEvents.push(startDate);
                    var weekNo = startDate.getWeekNumber();
                    if (cleanedEventsByWeek[weekNo]) {
                        cleanedEventsByWeek[weekNo].push({
                            id: key,
                            date:startDate
                        });
                    } else {
                        cleanedEventsByWeek[weekNo] = [{
                            id: key,
                            date:startDate
                        }];
                    }
                }
            }
        })

        //Get current Week and update initial view

        function initWeekDisplay(weekdays, w, y, cleanedEvents) {
            weekdays.find('.current').removeClass('current')
            weekdays.find('.has-event').removeClass('has-event')
            var d = (1 + (w) * 7);
            var innerDate = new Date(y, 0, d);
            var weekDays = new Array();
            innerDate.setDate((innerDate.getDate() - innerDate.getDay() +1));
            for (var i = 0; i < 7; i++) {
                weekDays.push(
                    new Date(innerDate)
                ); 
                innerDate.setDate(innerDate.getDate() +1);
            }
            weekdays.each(function(key, value) {
                value = jQuery(value);
                var dateNum = new Date(weekDays[key]).getDate();
                value.find('p').text(dateNum);
                //Add Class for today
                if (dateNum === today.getDate()) {
                    value.find('p').addClass('current');
                }
                //Add Link for active Event
                var found = cleanedEvents.filter(function(obj) {
                    var day = new Date(weekDays[key]);
                    return (obj.getFullYear()+''+('0' + (obj.getMonth()+1)).slice(-2)+''+('0' + obj.getDate()).slice(-2)) === (day.getFullYear()+''+('0' + (day.getMonth()+1)).slice(-2)+''+('0' + day.getDate()).slice(-2));
                })[0];
                if (found) {
                    value.find('p').addClass('has-event');
                    value.find('p').wrapInner('<a href="'+window.location.href.split('#')[0]+'#day-'+('0' + found.getDate()).slice(-2)+'"></a>')
                }
            });
            weekEventsMarkup.each(function(k, value) {
                value = jQuery(value);
                var dateData = value.data('covered-dates');
                var startDate = new Date(dateData.substr(10,4)+'-'+dateData.substr(7,2)+'-'+dateData.substr(4,2));
                if (dateData.indexOf('bis') >= 0) {
                    var endDate = new Date(dateData.substr(29,4)+'-'+dateData.substr(26,2)+'-'+dateData.substr(23,2));
                    var dates = getDates(startDate, endDate);
                    value.attr('data-covered-dates', dates.join(';'));
                } else {
                    value.attr('data-covered-dates', startDate);
                }
            });
        }

        initWeekDisplay(weekdays, currentWeekNumber, today.getFullYear(), cleanedEvents);

        //Hide Items not in current Week
        function updateMarkupByWeekNo(weekEventsMarkup, cleanedEventsByWeek, weekNo) {
            if (cleanedEventsByWeek[weekNo]) {
                weekEventsMarkup.each(function(k, value) {
                    var coveredDates = jQuery(value).attr('data-covered-dates').split(';');
                    var isInWeek = cleanedEventsByWeek[weekNo].filter(function(obj) {
                        return coveredDates.includes(obj.date.toString());
                    })[0];
                    if(isInWeek) {
                        jQuery(value).show();
                    } else {
                        jQuery(value).hide();
                    }
                });
            } else {
                weekEventsMarkup.hide();
            }
        }
        updateMarkupByWeekNo(weekEventsMarkup, cleanedEventsByWeek, currentWeekNumber);

        //Week nav
        weekNavPrev.click(function() {
            var targetWeek = Number(weekNavPrev.attr('data-target-week'));
            if (targetWeek === 1) {
                weekNavPrev.hide();
            }
            weekNavNext.show();
            weekNavPrev.attr('data-target-week', targetWeek - 1);
            weekNavNext.attr('data-target-week', targetWeek + 1);
            if (targetWeek === currentWeekNumber) {
                weekNavCurrent.text('aktuelle Woche');
            } else {
                weekNavCurrent.text('KW '+targetWeek);
            }
            initWeekDisplay(weekdays, targetWeek, today.getFullYear(), cleanedEvents);
            updateMarkupByWeekNo(weekEventsMarkup, cleanedEventsByWeek, targetWeek);
        })
        weekNavNext.click(function() {
            var targetWeek = Number(weekNavNext.attr('data-target-week'));
            if (isLeapYear && targetWeek === 53) {
                weekNavNext.hide();
            }
            if (!isLeapYear && targetWeek === 52) {
                weekNavNext.hide();
            }
            weekNavPrev.show();
            weekNavPrev.attr('data-target-week', targetWeek - 1);
            weekNavNext.attr('data-target-week', targetWeek + 1);
            if (targetWeek === currentWeekNumber) {
                weekNavCurrent.text('aktuelle Woche');
            } else {
                weekNavCurrent.text('KW '+targetWeek)
            }
            initWeekDisplay(weekdays, targetWeek, today.getFullYear(), cleanedEvents);
            updateMarkupByWeekNo(weekEventsMarkup, cleanedEventsByWeek, targetWeek);
        })

        //Month View
        var currentMonthDisplay = header.find('.current-month')
        var getMonth = window.location.search.split('&monat=')[1];
        if (getMonth) {
            var today = new Date;
            var currentMonth = today.getMonth();
            var month = Number(getMonth.substr(4,2)) - 1;
            var year = Number(getMonth.substr(0,4));
            if (currentMonth === month) {
                currentMonthDisplay.text('aktueller Monat');
            } else {
                switch (month) {
                    case 0:
                        displayMonth = 'Januar'
                        break;
                    case 1:
                        displayMonth = 'Februar'
                        break;
                    case 2:
                        displayMonth = 'M&auml;rz'
                        break;
                    case 3:
                        displayMonth = 'April'
                        break;
                    case 4:
                        displayMonth = 'Mai'
                        break;
                    case 5:
                        displayMonth = 'Juni'
                        break;
                    case 6:
                        displayMonth = 'Juli'
                        break;
                    case 7:
                        displayMonth = 'August'
                        break;
                    case 8:
                        displayMonth = 'September'
                        break;
                    case 9:
                        displayMonth = 'Oktober'
                        break;
                    case 10:
                        displayMonth = 'November'
                        break;
                    case 11:
                        displayMonth = 'Dezember'
                        break;                
                    default:
                        break;
                }
                currentMonthDisplay.html(displayMonth+' '+year);
            }
        } else {
            var today = new Date;
            var month = today.getMonth();
            var year = today.getFullYear();
        }
        if (month === 0) {
            var nextMonth = '02';
            var prevMonth = '12';
            var nextYear = year;
            var prevYear = year - 1;
        } else if (month === 11) {
            var nextMonth = '01';
            var prevMonth = '10';
            var nextYear = year + 1;
            var prevYear = year;
        } else {
            var nextMonth = ("0" + (month + 2)).slice(-2);
            var prevMonth = ("0" + (month)).slice(-2);
            var nextYear = year;
            var prevYear = year;
        }

        //Month nav
        var monthNavPrev = header.find('.month-nav .prev-month');
        var monthNavNext = header.find('.month-nav .next-month');
        var baseUrl = './'; //window.location.origin + '/' + window.location.pathname.split('/')[1];
        var queryDict = {}
        location.search.substr(1).split("&").forEach(function(item) {queryDict[item.split("=")[0]] = item.split("=")[1]})
        if (typeof queryDict["static"] !== "undefined") {
            var staticPage = queryDict["static"];
        } else {
            if (window.location.pathname === '/') {
                var staticPage = 'start';
            } else {
                var pagePath = window.location.pathname.split('/');
                var staticPage = pagePath[pagePath.length - 1].split('?')[0].split('&')[0];
            }
        }
        monthNavPrev.click(function() {
            window.location.href = baseUrl + "?static=" + staticPage + "&monat=" + prevYear + prevMonth;
        });
        monthNavNext.click(function() {
            window.location.href = baseUrl + "?static=" + staticPage + "&monat=" + nextYear + nextMonth;
        });

        var monthViewCalendar = jQuery('.month-view .event-calendar');
        if (monthViewCalendar.length > 0) {
            monthViewCalendar.find('.slot').each(function(k, value) {
                value = jQuery(value);
                var duration = value.data('duration');
                if (duration.indexOf('bis') >= 0) {
                    var dates = duration.split(' bis ');
                    var start = dates[0];
                    var end = dates[1];
                    var eventLength = daysDiff(
                        start.substr(4,2),
                        start.substr(7,2),
                        start.substr(10,4),
                        end.substr(4,2),
                        end.substr(7,2),
                        end.substr(10,4)
                    );
                    var voClass = value.data('vo-class');
                    var link = jQuery('<div>').append(value.find('a').clone()).html();
                    var startDate = new Date(start.substr(7,2)+'/'+start.substr(4,2)+'/'+start.substr(10,4));
                    var startWeekDay = -1;
                    switch (startDate.getDay()) {
                        case 0:
                            startWeekDay = 7;
                            break;                    
                        default:
                            startWeekDay = startDate.getDay();
                            break;
                    }
                    var remainingWeekDays = 8 - startWeekDay;
                    if (remainingWeekDays >= eventLength) {
                        //Fits in Current Week
                        value.addClass('days-'+eventLength);
                        var closestTD = value.closest('td');
                        closestTD.attr('data-running-events', Number(closestTD.attr('data-running-events')) + 1);
                        closestTD.nextAll('td').slice(0,remainingWeekDays).each(function(k, followerTD) {
                            followerTD = jQuery(followerTD);
                            followerTD.attr('data-running-events', Number(followerTD.attr('data-running-events')) + 1);
                        })
                    } else {
                        //Continute in next week
                        value.addClass('days-'+remainingWeekDays);
                        var closestTD = value.closest('td');
                        closestTD.attr('data-running-events', Number(closestTD.attr('data-running-events')) + 1);
                        closestTD.nextAll('td').slice(0,remainingWeekDays).each(function(k, followerTD) {
                            followerTD = jQuery(followerTD);
                            followerTD.attr('data-running-events', Number(followerTD.attr('data-running-events')) + 1);
                        })
                        var eventLength = eventLength - remainingWeekDays;
                        addEventToNextRow(value.closest('tr'), eventLength, voClass, link);
                    }                    
                } else {  
                    var closestTD = value.closest('td');                  
                    closestTD.attr('data-running-events', Number(closestTD.attr('data-running-events')) + 1);
                }
            })
            monthViewCalendar.find('td.has-event').each(function(k, value) {
                value = jQuery(value);
                var slots = value.find('.slot');
                if (slots.length > 1) {
                    slots.remove();
                    let orderedSlots = slots.sort(function(a, b) {
                        return String.prototype.localeCompare.call(b.className, a.className);
                    });
                    orderedSlots.each(function(k, value) {
                        value = jQuery(value);
                        value.removeClass(function(i, className) {
                            return (className.match(/\bslot-\S+/g) || []).join(' ');
                        });
                    });
                    value.append(orderedSlots);
                }
            })
            monthViewCalendar.find('tr:not(.head)').each(function(k, value) {
                value = jQuery(value);
                var slots = value.find('.slot');
                if (slots.length > 0) {
                    var maxSlots = 0;
                    slots.each(function(k, slot) {
                        slot = jQuery(slot);
                        var toBeFilledSlots = slot.closest('td').attr('data-running-events');
                        var tdSlotAmount = slot.closest('td').find('.slot').length;
                        if (tdSlotAmount > 1) {
                            for (let i = 0; i <= toBeFilledSlots; i++) {
                                slot.closest('td').find('.slot:nth-child('+(i + 1)+')').addClass('slot-'+i)
                            }
                        } else {
                            slot.addClass('slot-'+toBeFilledSlots)
                        }
                        if (maxSlots < toBeFilledSlots) {
                            maxSlots = toBeFilledSlots;
                        }
                    })
                    value.addClass('slot-amount-'+maxSlots);
                }
            })
        }

        function addEventToNextRow(currentTr, eventLength, voClass, link) {
            var nextRow = currentTr.next('tr');
            if (nextRow.length > 0) {
                var toInsert = nextRow.find('td:first-child');
                var remainingEventDays = eventLength;
                if (remainingEventDays > 7) {
                    toInsert.append('<div class="slot days-7 '+voClass+'">'+link+'</div>');
                    addEventToNextRow(toInsert.closest('tr'), eventLength - 7, voClass, link);
                    toInsert.attr('data-running-events', Number(toInsert.attr('data-running-events')) + 1);
                    toInsert.nextAll('td').each(function(k, followerTD) {
                        followerTD = jQuery(followerTD);
                        followerTD.attr('data-running-events', Number(followerTD.attr('data-running-events')) + 1);
                    })
                } else {
                    toInsert.append('<div class="slot days-'+remainingEventDays+' '+voClass+'">'+link+'</div>');
                    toInsert.attr('data-running-events', Number(toInsert.attr('data-running-events')) + 1);
                    toInsert.nextAll('td').slice(0,(remainingEventDays - 1)).each(function(k, followerTD) {
                        followerTD = jQuery(followerTD);
                        followerTD.attr('data-running-events', Number(followerTD.attr('data-running-events')) + 1);
                    })
                    return;
                }
            } else {
                return;
            }
        }

        function daysDiff(day1, month1, year1, day2, month2, year2) {
            var date1 = new Date(Date.UTC(year1, month1, day1, 12, 0, 0))
            var date2 = new Date(Date.UTC(year2, month2, day2, 12, 0, 0))
            var Difference_In_Time = date2.getTime() - date1.getTime();
            return Math.abs(Difference_In_Time / (1000 * 3600 * 24)) + 1;
        }


    }

    var loginLink = jQuery('.meta-nav .login-link');
    var loginModal = jQuery('.login-modal');
    var loginBackdrop = jQuery('.login-modal-backdrop')
    if (loginLink.length > 0 && loginModal.length > 0 && loginBackdrop.length > 0) {
        loginLink.click(function() {
            loginBackdrop.show();
            loginModal.show();
            jQuery('header, #wrap, footer, body').addClass('blur');
        })
        loginModal.find('.close').click(function() {
            loginBackdrop.hide();
            loginModal.hide();
            jQuery('header, #wrap, footer, body').removeClass('blur');
        })
        loginBackdrop.click(function() {
            loginBackdrop.hide();
            loginModal.hide();
            jQuery('header, #wrap, footer, body').removeClass('blur');
        })
    }

    /* removed due to {#MODULENAMESUB#}
    var title = jQuery('head title');
    if (title.length > 0) {
        title.text(removeElements(title.text(), 'i'));
    }
    var subNavItems = jQuery('.main-nav li, .header__sub-nav-wrapper li');
    if (subNavItems.length > 0) {
        subNavItems.each(function(k, value) {
            value = jQuery(value);
            if (value.find('i').length > 0) {
                value.find('.sub-nav__subtitle').text(value.find('i').text());
                value.find('i').remove();
            }
        })
    }
    */

    var mobileNavItemLink = jQuery('.mobile-nav__item-link');
    if (mobileNavItemLink.length > 0) {
        mobileNavItemLink.each(function(key, value) {
            value = jQuery(value);
            value.click(function(e) {
                if (value.parent().hasClass('is-selected') && value.attr('href').length > 0) {
                    window.location = value.attr('href');
                    return false;
                }
            })
        });
    }

    function removeElements(text, selector) {
        var wrapped = jQuery('<div>' + text + '</div>');
        wrapped.find(selector).remove();
        return wrapped.html();
    }

    var selects = jQuery('body.fe form select');
    if (selects.length > 0) {
        selects.each(function(key, value) {
            value = jQuery(value);
            var firstOption = value.find('option').first();
            if (firstOption.text().length === 0) {
                firstOption.attr('value', '')
                value.select2({
                    'minimumResultsForSearch': -1,
                    'width': '100%',
                    'placeholder': 'Bitte waehlen'
                }).on('select2:select', function(e) {
                    if (value.val() === '*') {
                        value.next('.select2').hide();
                    }
                })
            } else {
                value.select2({
                    'minimumResultsForSearch': -1,
                    'width': '100%'
                }).on('select2:select', function(e) {
                    if (value.val() === '*') {
                        value.next('.select2').hide();
                    }
                })
            }
        });
    }

    showASelect('#titelid');
    showASelect('#berufsfunktionid');
    showASelect('#brancheid');

    function showASelect(selector) {
        var item = jQuery(selector);
        if (item.length > 0) {
            item.find('a').click(function() {
                item.prev('.select2').show();
            })        
        }
    }

    var canContainCopyright = jQuery('.can-contain-copyright');
    if (canContainCopyright.length > 0) {
        canContainCopyright.each(function(key, value) {
            value = jQuery(value);
            var cr = canContainCopyright.find('.search-for-copyright i').first();
            if (cr.length > 0) {
                value.find('.card__image-credit').html('&copy; '+cr.text());
            } else {
                value.find('.card__image-credit').hide();
            }
        });
    }

    

    var consent = getCookie('bl_dsgvo_consent');
    if (!consent) {
        showPopup();
    }

    var dsgvoBlocked = jQuery('.dsgvo-blocked-info');
    if (dsgvoBlocked.length > 0) {
        var consent = getCookie('bl_dsgvo_consent');
        dsgvoBlocked.each(function(key, value) {
            value = jQuery(value);
            if (consent === 'external') {
                value.hide();
                value.next('.dsgvo-insert').html(value.next('.dsgvo-insert').data('insert'));
            }
        });
    }

    // append Opt Out to Datenschutz
    if (window.location.search === '?datenschutz') {
        jQuery('.contentbody').append('<h2>Cookies &amp; Skripte von Drittanbietern</h2><p>Sie haben zu einem fr&uuml;heren Zeitpunkt eine Auswahl zur Verwendung von Cookies &amp; Skripten von Drittanbietern getroffen</p><p class="dsgvo-consent-optout" style="display: block;"><a class="dsgvo-consent-optout-link">Klicken Sie hier um Ihre Auswahl zu &auml;ndern oder Ihre Zustimmung aufzuheben.</a></p>');
    }

    var consent = getCookie('bl_dsgvo_consent');
    var optOut = jQuery('.dsgvo-consent-optout');
    if (optOut.length > 0) {
        optOut.each(function(key, value) {
            value = jQuery(value);            
            value.find('.dsgvo-consent-optout-link').click(function() {
                dsgvoOptOut();
                showPopup();
            })
            if (!consent) {
                jQuery(value).hide();
            }
        });
    }
});

// from https://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
}

function dsgvoConsentSelected() {
    var popup = jQuery('.blueline-content-popup');
    var formID = popup.data('form-id');
    var baseUrl = window.location.origin + '/' + window.location.pathname.split('/')[1];
    var external = popup.find('input#blueline-content-external-scripts').prop('checked');
    var token = uuidv4();
    if (external) {
        jQuery.ajax({
            url: 'https://ip.wjd.de/',
            method: 'GET',
            success: function(data) {
                saveConsent(data.ip, baseUrl, 'external', formID, token);
            },
            error: function(err) {
                saveConsent('failed to get ip', baseUrl, 'external', formID, token);
            }
        });
        setCookie('external', token);
        checkForVideos();
        showOptOuts();
        hidePopup();
    } else {
        jQuery.ajax({
            url: 'https://ip.wjd.de/',
            method: 'GET',
            success: function(data) {
                saveConsent(data.ip, baseUrl, 'internal', formID, token);
            },
            error: function(err) {
                saveConsent('failed to get ip', baseUrl, 'internal', formID, token);
            }
        });
        setCookie('internal', token);
        hidePopup();
        showOptOuts();
    }
        
}
function dsgvoConsentAll() {
    var popup = jQuery('.blueline-content-popup');
    var formID = popup.data('form-id');
    var baseUrl = window.location.origin + '/' + window.location.pathname.split('/')[1];
    var token = uuidv4();
    jQuery.ajax({
        url: 'https://ip.wjd.de/',
        method: 'GET',
        success: function(data) {
            saveConsent(data.ip, baseUrl, 'external', formID, token);
        },
        error: function(err) {
            saveConsent('failed to get ip', baseUrl, 'external', formID, token);
        }
    });
    setCookie('external', token);
    checkForVideos();
    showOptOuts();
    hidePopup();
}

// borlabs reference {"consents":{"essential":["borlabs-cookie"],"statistics":["google-analytics"],"external-media":["googlemaps","vimeo","youtube","ecologi"]},"domainPath":"agentur-blueline.de/","expires":"Thu, 19 May 2022 19:07:36 GMT","uid":"sbp2qsfa-jq3b8kkn-rz2ko9ph-sel8lhrt","version":"1"}
function setCookie(consentValue, token) {    
    var cookie = {
        consent: consentValue,
        token: token
    };
    var cookieString = JSON.stringify(cookie);
    var now = new Date();
    now.setTime(now.getTime() + 1000 * 60 * 60 * 24 * 30 * 12); // 1 year
    document.cookie = 'bl_dsgvo_consent=' + cookieString + ';expires=' + now.toGMTString() + ';path=' + '/' + window.location.pathname.split('/')[1];
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) {
        var cookieString = parts.pop().split(";").shift();
        var cookie = JSON.parse(cookieString);
        return cookie.consent;
    } 
}

function dsgvoOptOut() {
    var now = new Date();
    now.setTime(now.getTime() - 1000);
    document.cookie = 'bl_dsgvo_consent=;expires=' + now.toGMTString() + ';path=' + '/' + window.location.pathname.split('/')[1];
    var optOut = jQuery('.dsgvo-consent-optout');
    if (optOut.length > 0) {
        optOut.each(function(key, value) {
            value = jQuery(value);
            value.hide();
        });
    }
}

function checkForVideos() {
    var videoEmbeds = jQuery('.video-embed');
    if (videoEmbeds.length > 0) {
        videoEmbeds.each(function(key, value) {
            value = jQuery(value);
            value.find('.dsgvo-insert').html(value.find('.dsgvo-insert').data('insert'));
        });
    } 
}
function hidePopup() {
    jQuery('.blueline-content-popup').hide();
}
function showPopup() {
    jQuery('.blueline-content-popup').show();
}
function showOptOuts() {
    jQuery('.dsgvo-consent-optout').show();
}

function saveConsent(ip, url, services, formID, token) {
    var form = new FormData();
    form.append('key_anon_ip', ip);
    form.append('key_services', services);
    form.append('key_token', token);
    form.append('id', formID);
    form.append('dialog', '1');
    form.append('cmd', 'addnew');
    form.append('action', 'data_umfrage');

    var settings = {
        'url': url+'/?umfrage=' + formID,
        'method': 'POST',
        'timeout': 0,
        'processData': false,
        'mimeType': 'multipart/form-data',
        'contentType': false,
        'data': form
    };

    jQuery.ajax(settings);
}

/**
 * yall
 */
document.addEventListener("DOMContentLoaded", function() {
    yall({
        observeChanges: true
    });
});