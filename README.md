# WJ-2021 Vereinonline Theme
### Wirtschaftsjunioren Deutschland e.V. 

Nutzt die Informationsseite https://plattform.wjd.de/fuer-kreise/vereinonline-design/ für
- Aktuelle Informationen und um das Template
- Fehlermeldungen
- Hilfeanfragen
- Anfragen für Weiterentwicklung
- Ihr bei dem Projekt mithelfen wollt

## Kompatibilität / Nutzungsempfehlung
Im Rahmen der Taskforce WJD Plattform wurden die Nutzungsszenarios für einen typischen Wirtschaftsjunioren Kreis evaluiert. Dabei wurde eine Kompatibilitäts- und Nutzenanalyse angefertigt. Die Ergebnisse gehen aus der folgenden Tabelle hervor. Es handelt sich hier lediglich um Empfehlungen, insbesondere aber auch um die Kompatbilität zu diesem Theme.

### Vereinonline Module

|  |Modul |Hinweise |
|---|---|---|
| ⚠️ | Startseite | Dieses Theme nutzt nicht die originale Vereinonline Startseite. Um eine individuelle Startseite anzulegen wird das Vorgehen, beschrieben in Abschnitt Layout empfohlen. |
| ⚡ | Startseite>Aktuelles | Nutzt hierzu das Template **MitSektionen** mit dem Abschnitt **Aktuelles** |
| ❌ | Startseite>Diskussionsforum | nicht verbreitet, daher kein Styling und nicht zur Nutzung empfohlen. |
| ❌ | Startseite>Blog | nicht verbreitet, daher kein Styling und nicht zur Nutzung empfohlen. |
| ⚡ | Veranstaltungen | Nutzt hierzu das Template **MitSektionen** mit dem Abschnitt **Veranstaltungen**  |
| ⚠️ | Bilder | nicht verbreitet, daher kein Styling, kann aber mit visuellen Einschränkungen genutzt werden. |
| ⚠️ | Online-Shop | nicht verbreitet, daher kein Styling, kann aber mit visuellen Einschränkungen genutzt werden. |
| ✔️ | Mitglieder | Primär für Administration, daher nicht gestyled aber nutzbar. |
| ✔️ | Mein Profil | Primär für Administration, daher nicht gestyled aber nutzbar. |
| ⚠️ | Platzbuchung |  nicht verbreitet, daher kein Styling, kann aber mit visuellen Einschränkungen genutzt werden. |
| ⚠️ | Datenablage | nicht verbreitet, daher kein Styling, kann aber mit visuellen Einschränkungen genutzt werden. |
| ⚠️ | Aufgaben | nicht verbreitet, daher kein Styling, kann aber mit visuellen Einschränkungen genutzt werden. |
| ✔️ | Kasse | Primär für Administration, daher nicht gestyled aber nutzbar. |
| ✔️ | Administration | Primär für Administration, daher nicht gestyled aber nutzbar. |


### Kompatibilität zu vorherigen Vereinonline / WJ Layouts
Es wird Empfohlen vor Umschaltung die Inhalte der bisherigen Seite zu sichern.
#### WJ / WJ-Bayern-2015
Da dieses Template die Sektionen nicht vollumfänglich nutzt ist eine Kompatibilität grundsätzlich immer vorhanden. **Es wird jedoch dringend empfohlen die Inhalte gemäß dieser Dokumentation neu aufzubereiten!**

#### WJ-2017
Da dieses Template die Sektionen nicht vollumfänglich nutzt ist eine Kompatibilität grundsätzlich immer vorhanden. **Es wird jedoch dringend empfohlen die Inhalte gemäß dieser Dokumentation neu aufzubereiten!** Die Seitentypen wurden namentlich aus diesem Template übernommen, sodass die Seiten weiterhin verfügbar sind. Dennoch ist das Layout visuell anders strukturiert.

# Layout Grundeinrichtung
## Schritt 1: Layout auswählen
Zuerst muss das neue Design **WJ-2021** ausgewählt werden. Diese Einstellung kann nur durch einen **Administrator** erfolgen. Man geht hierzu auf den Menüpunkt **Administratrion** > **Basiskonfiguration** > **Layout**. Hier kann man aus der Auswahlliste nun das Layout **WJ-2021** auswählen. Eine Vorabversion, welche nur zu Test-/Entwicklungszwecken genutzt werden sollte ist unter **WJ-2021-DEV** zu finden. Nach der Auswahl des Layouts bestätigt man die Auswahl mit **Speichern**.

![Schritt 1](doc/basic_step1.png "Schritt 1")

Das Ergebnis sieht in einer Blankoinstallation dann wie folgt aus

![Schritt 1](doc/basic_step1_result.png "Schritt 1 Ergebnis")

## Schritt 2: Vereinonline Einstellungen
### Schritt 2.1: Basiskonfiguration
Unter dem Administrationsmenü ist unter **Administratrion** > **Basiskonfiguration** > **Parameter** folgender Eintrag einzufügen:

```
html.static.standardlinks.anlegen=ja
html.static.standardlinks.rechte=ja
```

Nach Eingabe mit **Speichern** bestätigen. Diese Parameter erlauben eine erweiterte Bearbeitung der Menüstruktur.

### Schritt 2.2: Logo Upload
Das Template sieht die Nutzung von einem Logo im **SVG Format** in den Abmessungen **389px x 108px** vor. Alternativ kann auch ein **PNG** Logo in Betracht gezogen werden. Das Logo sollte in **weiß** mit **transparentem Hintergrund** angelegt sein. Eure entsprechende Logo-Vorlage, welche dem WJD-Corporate Design entspricht erhaltet ihr auch über die Bundesgeschäftsstelle.

Um das Logo hochzuladen, geht in das Administrationsmenü unter **Administratrion** > **Basiskonfiguration** > **Dateien**. Klickt auf **Datei hinzufügen**, wählt das Logo aus und bestätigt mit **Änderungen Speichern**. Um die URL des Logos zu erhalten, klickt mit der rechten Maustaste auf den Link und wählt Adresse des Links kopieren. Diese wird im nächsten Schritt benötigt.

![Schritt 2](doc/basic_step_2_2.png "Schritt 2")

### Schritt 2.3: Konfiguration Cookie Consent Modul
Um den rechtssicheren Betrieb der Instanz zu gewährleisten wird es mit einem Cookie Consent Modul ausgeliefert. Dieser Abschnitt beschreibt die Konfiguration des Cookie Consent Banners, welcher u.a. die Einbindung von externen Videos datenschutzkonform ermöglicht.
*Hinweis: Die Wirtschaftsjunioren Deutschland e.V. übernehmen keine Haftung im Rahmen der Nutzung dieses Templates. Die rechtliche Konformität ist durch den Seitenbetreiber sicherzustellen.*

Um das Cookie Consent Modul zu konfigurieren, muss in Vereinonline eine **Flexible Liste** in welcher die Daten erfasst werden angelegt werden. Hierzu geht man in das Administrationsmenü unter **Administration** > **Flexible Listen**. 

![Schritt 2](doc/basic_step_2_3.png "Schritt 2")

Zuerst muss ein neuer Typ angelegt werden. Klickt dafür auf **Typen** und anschließend auf **Neuen Typen hinzufügen**.

![Schritt 2](doc/basic_step_2_3_type.png "Schritt 2")

Gebt dem Typen den Namen **Consent** und bestätigt mit **Speichern**. Anschließend können die Felder angelegt werden. Bitte legt die Felder **exakt** so an, wie auf der Abbildung. Wenn der Feldname nicht übereinstimmt ist die Funktionalität nicht gegeben. Im Anschluss nochmals mit Speichern bestätigen.

![Schritt 2](doc/basic_step_2_3_new.png "Schritt 2")

Nachdem der Typ angelegt ist kann in der Navigation über **Flexible Listen** und **Neue Liste hinzufügen** eine neue flexible Liste angelegt werden.

![Schritt 2](doc/basic_step_2_3_list.png "Schritt 2")

Diese Name erhält ebenfalls den Namen **Consent** und den zuvor angelegten Typ **Consent**. Wichtig ist auch der Haken bei der Option **öffentliche Umfrage**. Danach kann die Liste gespeichert werden. 

![Schritt 2](doc/basic_step_2_3_id.png "Schritt 2")

Zuletzt müssen wir die "ID" der Liste notieren. Hierzu klicken wir auf den Namen der Liste. Im sich nun öffnenden Fenster ist die "ID" in der Adressleiste zu entnehmen. (Siehe Abbildung). Diese ID benötigen wir im darauffolgenden Schritt "Layout".

### Schritt 2.4: Layout
Unter dem Administrationsmenü sind unter **Administratrion** > **Basiskonfiguration** > **Layout** > **Layout-Basiseinstellung** folgende Einstellungen zu tätigen:
| Parameter | Beispiel-Wert | Beschreibung |
|---|---|---|
|Logo URL|https://vo.wjd.de/APPR/files/logo.svg|Logo URL siehe Schritt 2.2|
|Header Button Link|?action=members_mitgliedsantrag|URL für Call-to-Action Button im Header|
|Header Button Text|Mitglied werden|Text für Call-to-Action Button im Header|
|Footer Button Link|?action=members_mitgliedsantrag|URL für Call-to-Action Button im Footer|
|Footer Button Text|Mitglied werden|Text für Call-to-Action Button im Header|
|Footer Link Url1|https://google.de|Möglichkeit für vierten Link im Footer-Menü neben Kontakt, Impressum und Datenschutz|
|Footer Link Name1|Google|Möglichkeit für vierten Link im Footer-Menü neben Kontakt, Impressum und Datenschutz|
|Footer Link Url2|https://google.de|Möglichkeit für fünften Link im Footer-Menü neben Kontakt, Impressum und Datenschutz|
|Footer Link Name2|Google|Möglichkeit für fünften Link im Footer-Menü neben Kontakt, Impressum und Datenschutz|
|Facebook|https://www.facebook.com/Wirtschaftsjunioren|URL zum Facebook-Profil für Social Icon|
|Instagram|https://www.instagram.com/wj_deutschland/|URL zum Instagram-Profil für Social Icon|
|Twitter|https://twitter.com/WJDeutschland|URL zum Twitter-Profil für Social Icon|
|Linked In|https://www.linkedin.com/company/junge-wirtschaft/|URL zum LinkedIn-Profil für Social Icon|
|Cookie Consent ID|123|ID der flexiblen Liste für Cookie Consent siehe Schritt 2.3|
|Hero Hoehe|700|Höhe in px des Hero Sliders (Desktop)|
|Hero Hoehe Responsive|350|Höhe in px des Hero Sliders (Responsive)|
|Footer Partner Ausblenden|1|1 blendet Partnerbereich in der Fußzeile aus, leer blendet Partnerbereich ein|
|Karten Hoehe Bilder33|200|Höhe in Pixel für Bilder in Kartensammlung 33/33/33|
|Karten Hoehe Bilder50|200|Höhe in Pixel für Bilder in Kartensammlung 50/50|
|Karten Hoehe Bilder Mitglieder|500|Höhe in Pixel für Bilder in Sektion Mitglieder|

### Schritt 2.5: Partner-Logos
Es können bis zu drei Partner-Logos hinterlegt werden. Hierzu könnt ihr unter **Administratrion** > **Basiskonfiguration** > **Layout** > **Logo / Bilder** einfach grafiken hochladen.

### Schritt 2.6: Rollenmanagement
Das Rollenmanagement ist essentieller Bestandteil in der Verwaltung von Vereinonline. Siehe hierzu auch: https://www.vereinonline.org/faq?rollen 

Jeder sollte sich Gedanken über eine adäquate Rollenstruktur machen. Es gilt die Regel: Nur die Rollen vergeben, welche auch benötigt werden!

Um einen Betrieb der Vereinonline Instanz auch für nicht angmeldete Nutzer zu ermöglichen müssen zwei Einstellungen getroffen werden. Hierzu öffnet man das Rollen/Rechte System unter **Administration** > **Rollen**.

![Schritt 2](doc/basic_step_2_5.png "Schritt 2")

Durch klicken auf den Bleistift bei der Rolle **nicht angemeldet** öffnet sich ein Dialog. Hier sind folgende Einstellungen zu treffen:
- Veranstaltungen: lesen
- Veranstaltungen anmelden: anmelden

Dies ermöglicht externen Teilnehmern den Kalender zu sehen und sich für Veranstaltungen anzumelden. Im Anschluss mit **Speichern** bestätigen.

## Schritt 3: Webseite verwalten

*Work In Progress here*

## Wichtige Informationen
### Kalender auf Startseite

Um eine korrekte Funktion des Abschnitts **Veranstaltungen** auf der Startseite zu gewährleisten muss die Startseite mit dem Namen **start** angelegt werden.

- Standardseitenstruktur anlegen
- Vereinonline Menüpunkte auf Sichtbarkeit "Spezial" stellen
- Neue Seiten mit Template "MitSektionen" anlegen und Inhalte in Abschnitte




# Backlog
- Partner Logos Details



